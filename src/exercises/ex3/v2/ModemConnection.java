package exercises.ex3.v2;

public interface ModemConnection {
     ModemConnection dial(String pno);
    ModemDataReciever getReceiver();
    ModemDataSender getSender();
     void hangup();
}
