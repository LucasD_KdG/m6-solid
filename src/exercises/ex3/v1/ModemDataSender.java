package exercises.ex3.v1;

public interface ModemDataSender
{
     void send(char c);
}
