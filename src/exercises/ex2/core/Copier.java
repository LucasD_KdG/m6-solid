package exercises.ex2.core;

public class Copier {

	public void copy(Reader r, Writer w) {

		while (r.hasNext()) {
			w.write(r.read());
		}

	}
}