package exercises.ex2.core;

public interface Writer {
    public void write(char i);
}
