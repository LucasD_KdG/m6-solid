package exercises.ex1;

public class LeakingSecurityService implements ISecurityService {
    @Override
    public boolean checkAccess(User user) {
        System.out.println("Van mij krijgt iedereen toegang.    ");
        return true;
    }
}
