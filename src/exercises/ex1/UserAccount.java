package exercises.ex1;

public class UserAccount {
    private ISecurityService sec;

    public UserAccount(ISecurityService sec) {
        this.sec = sec;
    }

    public void changeEmail(User u){
        if(sec.checkAccess(u)){
            System.out.println("De gebruiker zijn e-mail wordt aangepast.");
        }
    }
}
