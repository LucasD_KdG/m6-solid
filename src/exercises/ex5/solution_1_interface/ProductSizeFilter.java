package exercises.ex5.solution_1_interface;

import exercises.ex5.*;

import java.util.ArrayList;
import java.util.List;

public class ProductSizeFilter implements ProductFilter
{
    private ProductSize productSize;

    public ProductSizeFilter(ProductSize productSize){
        this.productSize = productSize;
    }


	@Override
	public List<Product> filter(List<Product> products) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product:products)
		{
			if (product.getSize() == productSize)
			{filteredProducts.add(product);}
		}
		return filteredProducts;	}
}
