package exercises.ex5.solution_1_interface;

import exercises.ex5.*;


public class Runner {

    // OCP: Make an interface ProductFilter and add implementations
    // ISP: there is a specific filter for each filter criterion
    public static void main(String[] args) {

        ProductFilter cf = new ProductColorFilter(ProductColor.RED);
        System.out.println("Red products: "+ cf.filter(Product.getProducts()));
        ProductFilter sf = new ProductSizeFilter(ProductSize.M);
        System.out.println("M products: "+ sf.filter(Product.getProducts()));
        ProductFilter af = new ProductColorAndSizeFilter(ProductColor.RED,ProductSize.M);
        System.out.println("Red M products: "+ af.filter(Product.getProducts()));

    }
}
