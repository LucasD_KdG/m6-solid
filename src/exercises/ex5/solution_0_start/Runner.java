package exercises.ex5.solution_0_start;

import exercises.ex5.Product;


import static exercises.ex5.ProductColor.GRAY;
import static exercises.ex5.ProductSize.S;


public class Runner {
    // Partial solution: only one filter (at a time)
    // OCP: Make an interface for the filter and provide implementations
    // ISP: have a dedicated filter for each filtering criterion
    public static void main(String[] args) {

        ProductFilter pf = new ProductFilter();
        System.out.println("Gray products: "+pf.filterByColor(Product.getProducts(), GRAY));
        System.out.println("S products: "+pf.filterBySize(Product.getProducts(), S));
        System.out.println("Gray S products: "+pf.filterByColorAndSize(Product.getProducts(), GRAY,S));
    }
}
