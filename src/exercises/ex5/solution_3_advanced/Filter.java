package exercises.ex5.solution_3_advanced;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Filter<T> {
	public List<T> filter(List<T> items, Predicate<T> pt) {
		List<T> filtered = new ArrayList<>();
		for (T item : items) {
			if (pt.test(item)) {filtered.add(item);}
		}
		return filtered;
	}
}
