package exercises.ex5.solution_3_advanced;


import exercises.ex5.Product;
import exercises.ex5.ProductColor;
import exercises.ex5.ProductSize;
import java.util.function.Predicate;

public class Runner {

  // This builds further on the SOID solution and combines several techniques to build a more general solution
  // - generics: Filter works on any type
  // - lambda's for specifying tests
  // - Predicate (a functional interface returning a boolean), which also supports methods like and(otherPredicate) for combining predicates

  public static void main(String[] args) {
    Filter<Product> pf = new Filter<>();
    Predicate<Product> productColorTest = p -> p.getColor() == ProductColor.RED;
    Predicate<Product> productSizeTest = p -> p.getSize() == ProductSize.XXL;
    Predicate<Product> productAndTest = productColorTest.and(productSizeTest);
    System.out.println("Red Products " + pf.filter(Product.getProducts(), productColorTest));
    System.out.println("XXL Products " + pf.filter(Product.getProducts(), productSizeTest));
    System.out.println("Red XXL Products " +
        pf.filter(Product.getProducts(), productColorTest.and(productSizeTest)));
  }
}
