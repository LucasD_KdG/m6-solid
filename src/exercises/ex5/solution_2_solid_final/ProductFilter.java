package exercises.ex5.solution_2_solid_final;


import exercises.ex5.Product;

import java.util.ArrayList;
import java.util.Collection;

public class ProductFilter {
	public Collection<Product> filter(Collection<Product> products, ProductTest pt) {
		Collection<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (pt.match(product)) {filteredProducts.add(product);}
		}
		return filteredProducts;
	}
}
