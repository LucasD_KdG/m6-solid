package exercises.ex5.solution_2_solid_final;

import exercises.ex5.*;

public class ProductSizeTest implements ProductTest {
	private ProductSize productSize;

	public ProductSizeTest(ProductSize size) {
		productSize = size;
	}

	@Override
	public boolean match(Product product) {
		return product.getSize() == productSize;
	}
}
